# Install script for directory: /Users/bgear/Downloads/GluPlay/CocosTest

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "DEBUG")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/external/chipmunk/src/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/external/Box2D/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/external/unzip/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/external/tinyxml2/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/audio/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/2d/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/network/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/extensions/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/editor-support/spine/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/editor-support/cocosbuilder/cmake_install.cmake")
  include("/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/editor-support/cocostudio/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/Users/bgear/Downloads/GluPlay/CocosTest/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
