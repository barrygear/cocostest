# README #

This is the CocosTest app for GluPlay.

### The story so far ###

Requirements:

1. [Add user input via Mouse/Touch handlers](#markdown-header-user-input)
1. [Move character to event point](#markdown-header-character-movement)
1. [Avoid Tables](#markdown-header-avoid-tables)
1. [Add pathfinding](#markdown-header-pathfinding)
1. [Randomize Tables](#markdown-header-randomize-tables)
1. [Ensure no grid is blocked by the tables](#markdown-header-randomize-tables-validation)

## User Input
The requirements say to add Touch, however, you will need a touch enabled deployment target to test that code. This implies that adding iOS as a target may be additional goal.

I started on an iOS target and am currently held up due to dependant libraries not containing an amd64 chunk (libcurl and libwebsockets, maybe more). Removing amd64 from the build caused other issues and it feels like a rabbit hole. At that point I feel that I can get the iOS target functional I think that creating mouse events may be sufficient since getting Touch working would take a while recompiling separate modules.

I added Mouse and Touch (untested yet) event handlers that register and log the events. After adding the mouse handler, looking at the debug output you can see that its returning absolute coordinates from the screen position and game object are in a differnt coordinate system. The gamescreens coordinate system is Y-reversed as well as can be a different scale when deployed into a HighDPI environment. I converted the mouse coordinates to game coordinates using this:
```c++
	Point mouseLocation = Point(e->getCursorX(), -e->getCursorY());
	Point nodeClickPos = Director::getInstance()->convertToUI(mouseLocation);
```
The convertToUI does coordinate translations and hopefully thats sufficient for multiple resolutions and DPIs.

Secondly, since I need to determine what was clicked I added the mouse handler to both the grid elements and the Table elements. Ideally, when an item is selected, its object is passed to the event handler and you know exactly whats selected. I registered the handler using SceneGraph priority so it will pass the highest z-ordered element first. This works great for the Tables, i.e. whenever a table is selected, the event handler can detect that its a table and consume the event. However, when a tile is selected, they are all the same z-order (since thats how there were inserted into the scene) an event will be sent to each one. In order to determine what tile was selected, during each event callback the mouse coordinates are compared to the tiles bounding area. If the mouse click is within the tile, then its selected and the event can be consumed. Selecting a non Table element sends a custom event message to the Character to reposition itself to the selected tile.

Since I've been refreshing my memory for Cocos2D knowledge, the code is mostly development level. For a production title, the events (either Mouse, Touch, or Custom) should be handled through a central event handler since having such code spread out amongst objects makes it hard to manage when there are tens or hundreds of events.

I've added the NodeData class to the Node objects' UserData payload to contain a common set of context variables. An alternate way is to extend the Node objects to define the object with the new values (i.e. create a FloorTile class and Table class by extending Node). Adding to the inheritance tree of an object can be considered adding additional cognitive load and complexity. First attempts to solve issues should, generally, be the simplest approach.

TODO : 

1. Clean up and create a professional event manager
1. Possibly create an iOS build to test Touch
1. Unit tests for event handler
1. Clean up & documentation

## Character Movement

Currently the character slides in a linear fashion to whatever tile the user selects with touches that land on a Table being discarded. 

TODO :

1. Center the character to the + on the tile grid
1. Have the character walk from tile to tile using simple 2d translation animation
	* Requires the pathfinding system
1. Clean up & documentation

## Avoid Tables

Currently you cant select a table as a destination to move to. It does this by detecting the object type the user has selected and discarding (and consuming) the event.

TODO:

1. Ensure tables are avoided in the pathfinding results
1. Clean up & documentation

## Pathfinding

I'm working an a Breadth First Search style pathfinding algorithm. Since the grid is can be viewed as a set of 3x3 tile sets with the character in the center my algorithm will be:

* Check surrounding tiles first, for each tile in 3x3 grid surrounding 
	* If tile is invalid (i.e. an edge tile), is Table tile, or is character tile then ignore (mark as invalid)
	* If tile is target, return tileId, depth pair
	* If tile is not target the mark with current tileId and depth in target tile, store for 2nd pass
* for each marked tile, recurse by calling search on new tileId
	* If recursion result is not null, add current tileId, return result
	* If target not found return null

I added a* search by requst. Its better at determining the direction to move initially but its not perfect. It still takes less than straight steps to the destination. 

BFS searchs is more efficient if the target destination is close by. Where a* calculates all of the grid locations first, even if the target is right next to the character, however, since it does that it finds the most efficient path afterwards.

TODO:

1. Implement recursion
1. Add unit tests 
1. Clean up & documentation

## Randomize Tables

This should be a matter of:

1. Replace Tables with Grid Tiles, erasing them from the 'board'
1. Randomize the GridCoordinate of the tables
1. Restore the tables back to the grid on the same z-order.
1. Ensure the character is highest z-order

TODO: DONE

## Randomize Tables Validation

My current plan for this is:

1. After tables are randomly placed back into the grid, first first empty grid, starting search from character position
1. From that starting position, use pathfinding to search all remaining tiles, ignoring tables

TODO: DONE