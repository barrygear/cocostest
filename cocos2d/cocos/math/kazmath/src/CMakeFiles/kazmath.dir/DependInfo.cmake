# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/GL/mat4stack.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/GL/mat4stack.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/GL/matrix.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/GL/matrix.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/aabb.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/aabb.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/mat3.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/mat3.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/mat4.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/mat4.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/plane.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/plane.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/quaternion.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/quaternion.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/ray2.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/ray2.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/utility.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/utility.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/vec2.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/vec2.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/vec3.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/vec3.c.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/vec4.c" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/math/kazmath/src/CMakeFiles/kazmath.dir/vec4.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/GLFW"
  "cocos2d"
  "cocos2d/cocos"
  "cocos2d/cocos/audio/include"
  "cocos2d/cocos/2d"
  "cocos2d/cocos/2d/renderer"
  "cocos2d/cocos/2d/platform"
  "cocos2d/cocos/2d/platform/desktop"
  "cocos2d/cocos/2d/platform/linux"
  "cocos2d/cocos/base"
  "cocos2d/cocos/physics"
  "cocos2d/cocos/editor-support"
  "cocos2d/cocos/math/kazmath/include"
  "cocos2d/extensions"
  "cocos2d/external"
  "cocos2d/external/edtaa3func"
  "cocos2d/external/jpeg/include/linux"
  "cocos2d/external/tiff/include/linux"
  "cocos2d/external/webp/include/linux"
  "cocos2d/external/tinyxml2"
  "cocos2d/external/unzip"
  "cocos2d/external/chipmunk/include/chipmunk"
  "cocos2d/external/freetype2/include/linux"
  "cocos2d/external/linux-specific/fmod/include/64-bit"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
