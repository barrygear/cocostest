# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCAffineTransform.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCAffineTransform.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCArray.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCArray.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCAutoreleasePool.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCAutoreleasePool.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCConsole.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCConsole.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCData.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCData.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCDataVisitor.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCDataVisitor.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCDictionary.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCDictionary.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCGeometry.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCGeometry.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCNS.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCNS.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCObject.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCObject.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCSet.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCSet.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCString.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCString.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CCValue.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/CCValue.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/atitc.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/atitc.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/etc1.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/etc1.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/s3tc.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/base/CMakeFiles/cocosbase.dir/s3tc.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/GLFW"
  "cocos2d"
  "cocos2d/cocos"
  "cocos2d/cocos/audio/include"
  "cocos2d/cocos/2d"
  "cocos2d/cocos/2d/renderer"
  "cocos2d/cocos/2d/platform"
  "cocos2d/cocos/2d/platform/desktop"
  "cocos2d/cocos/2d/platform/linux"
  "cocos2d/cocos/base"
  "cocos2d/cocos/physics"
  "cocos2d/cocos/editor-support"
  "cocos2d/cocos/math/kazmath/include"
  "cocos2d/extensions"
  "cocos2d/external"
  "cocos2d/external/edtaa3func"
  "cocos2d/external/jpeg/include/linux"
  "cocos2d/external/tiff/include/linux"
  "cocos2d/external/webp/include/linux"
  "cocos2d/external/tinyxml2"
  "cocos2d/external/unzip"
  "cocos2d/external/chipmunk/include/chipmunk"
  "cocos2d/external/freetype2/include/linux"
  "cocos2d/external/linux-specific/fmod/include/64-bit"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
