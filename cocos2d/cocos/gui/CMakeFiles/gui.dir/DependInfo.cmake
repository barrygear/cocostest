# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CocosGUI.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/CocosGUI.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIButton.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIButton.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UICheckBox.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UICheckBox.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIHelper.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIHelper.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIImageView.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIImageView.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UILayout.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UILayout.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UILayoutDefine.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UILayoutDefine.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UILayoutParameter.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UILayoutParameter.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIListView.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIListView.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UILoadingBar.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UILoadingBar.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIPageView.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIPageView.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIScrollView.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIScrollView.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UISlider.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UISlider.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIText.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIText.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UITextAtlas.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UITextAtlas.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UITextBMFont.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UITextBMFont.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UITextField.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UITextField.cpp.o"
  "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/UIWidget.cpp" "/Users/bgear/Downloads/GluPlay/CocosTest/cocos2d/cocos/gui/CMakeFiles/gui.dir/UIWidget.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CC_ENABLE_CHIPMUNK_INTEGRATION=1"
  "LINUX"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/GLFW"
  "cocos2d"
  "cocos2d/cocos"
  "cocos2d/cocos/audio/include"
  "cocos2d/cocos/2d"
  "cocos2d/cocos/2d/renderer"
  "cocos2d/cocos/2d/platform"
  "cocos2d/cocos/2d/platform/desktop"
  "cocos2d/cocos/2d/platform/linux"
  "cocos2d/cocos/base"
  "cocos2d/cocos/physics"
  "cocos2d/cocos/editor-support"
  "cocos2d/cocos/math/kazmath/include"
  "cocos2d/extensions"
  "cocos2d/external"
  "cocos2d/external/edtaa3func"
  "cocos2d/external/jpeg/include/linux"
  "cocos2d/external/tiff/include/linux"
  "cocos2d/external/webp/include/linux"
  "cocos2d/external/tinyxml2"
  "cocos2d/external/unzip"
  "cocos2d/external/chipmunk/include/chipmunk"
  "cocos2d/external/freetype2/include/linux"
  "cocos2d/external/linux-specific/fmod/include/64-bit"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
