//
//  Tags.h
//  CocosTest
//
//  Created by Barry Gear on 11/15/16.
//
//

#ifndef Tags_h
#define Tags_h


#define kGridWidth 10
#define kGridHeight 5

#define TAG_DEBUG		10
#define TAG_MOUSE_DEBUG	11
#define TAG_SCENE		100
#define TAG_TABLE		200
#define TAG_CHARACTER	1000
#define TAG_TILE		10000

#endif /* Tags_h */
