//
//  FindablePriorityQueue.hpp
//  CocosTest
//
//  Created by Barry Gear on 11/18/16.
//
//

#ifndef FindablePriorityQueue_hpp
#define FindablePriorityQueue_hpp

#include "NodeInfo.h"
#include <queue>

template<
class T,
class Container = std::vector<T>,
class Compare = std::less<typename Container::value_type>
> class FindablePriorityQueue : public std::priority_queue<T, Container, Compare>
{
public:
	typedef typename std::priority_queue<T,Container,Compare>::container_type::const_iterator const_iterator;
	
	const_iterator find(const T&val) const
	{
		auto first = this->c.cbegin();
		auto last = this->c.cend();
		while (first!=last) {
			if (*first==val) return first;
			++first;
		}
		return last;
	}

	bool contains(const T&val) const
	{
		auto first = this->c.cbegin();
		auto last = this->c.cend();
		while (first!=last) {
			if (*first==val)
				return true;
			++first;
		}
		return false;
	}
	
};

#endif /* FindablePriorityQueue_hpp */
