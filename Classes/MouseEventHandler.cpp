//
//  MouseEventHandler.cpp
//  CocosTest
//
//  Created by Barry Gear on 11/14/16.
//
//

#include "MouseEventHandler.h"

USING_NS_CC;

#define EVENT_MOUSE_DOWN_STR	"Mouse Down Detected"
#define EVENT_MOUSE_UP_STR		"Mouse Up Detected"
#define EVENT_MOUSE_MOVE_STR	"Mouse Move Detected"
#define EVENT_MOUSE_SCROLL_STR	"Mouse Scroll Detected"

void MouseEventHandler::onMouseDown(Event *event)
{
//	EventMouse* e = dynamic_cast<EventMouse*>(event);
//	
//	Point mouseLocation = Point(e->getCursorX(), -e->getCursorY());
//	Point nodeClickPos = Director::getInstance()->convertToUI(mouseLocation);
//	
//	auto bounds = e->getCurrentTarget()->getBoundingBox();
//	if (bounds.containsPoint(nodeClickPos)){
//		if(e->getCurrentTarget()->getUserData()) {
//			CCLOG("Event: %s(%i) Target:(%5.2f,%5.2f)", EVENT_MOUSE_DOWN_STR, e->getMouseButton(), e->getCurrentTarget()->getPositionX(), e->getCurrentTarget()->getPositionY());
//
//			CCLOG("Node: %i, %li", ((NodeInfo *)e->getCurrentTarget()->getUserData())->type, ((NodeInfo *)e->getCurrentTarget()->getUserData())->order);
//			
//			EventCustom event("move_character");
//			event.setUserData(e->getCurrentTarget());
//			Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
//			
//		} else {
//			CCLOG("Node: null");
//		}
//	}
}

void MouseEventHandler::onMouseUp(Event *event)
{
	EventMouse* e = dynamic_cast<EventMouse*>(event);
	
	Point mouseLocation = Point(e->getCursorX(), -e->getCursorY());
	Point nodeClickPos = Director::getInstance()->convertToUI(mouseLocation);
	
	auto bounds = e->getCurrentTarget()->getBoundingBox();
	if (bounds.containsPoint(nodeClickPos)){
		if(e->getCurrentTarget()->getUserData()) {
			//CCLOG("Event: %s(%i) Target:(%5.2f,%5.2f)", EVENT_MOUSE_UP_STR, e->getMouseButton(), e->getCurrentTarget()->getPositionX(), e->getCurrentTarget()->getPositionY());
			//CCLOG("Node: %i, %li", ((NodeInfo *)e->getCurrentTarget()->getUserData())->type, ((NodeInfo *)e->getCurrentTarget()->getUserData())->ordinal);
			if(((NodeInfo *)e->getCurrentTarget()->getUserData())->type == NODE_TYPE_TILE) {
				EventCustom * event = new EventCustom(MOVE_CHARACTER);
				event->setUserData(e->getCurrentTarget());
				Director::getInstance()->getEventDispatcher()->dispatchEvent(event);
			}
		} else {
			CCLOG("Node: null");
		}
		e->stopPropagation();
	}
}

void MouseEventHandler::onMouseMove(Event *event)
{
	EventMouse* e = dynamic_cast<EventMouse*>(event);
	Point mouseLocation = Point(e->getCursorX(), -e->getCursorY());
	Point uiClickPos = Director::getInstance()->convertToUI(mouseLocation);
	Point convertedPoint = e->getCurrentTarget()->convertToNodeSpace(mouseLocation);
    
	auto bounds = e->getCurrentTarget()->getBoundingBox();
	if (bounds.containsPoint(uiClickPos)){
		Node *scene = Director::getInstance()->getRunningScene()->getChildByTag(TAG_SCENE);
		LabelTTF *debug = (LabelTTF *)scene->getChildByTag(TAG_DEBUG);
        LabelTTF *mouseDebug = (LabelTTF *)scene->getChildByTag(TAG_MOUSE_DEBUG);
        if(e->getCurrentTarget()->getUserData()) {
            std::string str = StringUtils::format("Node: [%i][%li]",
                    ((NodeInfo *)e->getCurrentTarget()->getUserData())->type,
                    ((NodeInfo *)e->getCurrentTarget()->getUserData())->ordinal);
            debug->setString(str);
            
            str = StringUtils::format("(%2.0f,%2.0f)|(%2.0f,%2.0f)|(%2.0f,%2.0f)",
                                      e->getCursorX(), e->getCursorY(),
                                      uiClickPos.x, uiClickPos.y,
                                      convertedPoint.x, convertedPoint.y);
            mouseDebug->setString(str);
		} else {
			debug->setString("");
		}
		e->stopPropagation();
	}
}

void MouseEventHandler::onMouseScroll(Event *event)
{
	EventMouse* e = (EventMouse*)event;
//	CCLOG("Event: %s : (%5.2f,%5.2f)", EVENT_MOUSE_SCROLL_STR, e->getScrollX(), e->getScrollY());
	e->stopPropagation();
}
