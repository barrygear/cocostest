#ifndef __ENGTEST_SCENE_H__
#define __ENGTEST_SCENE_H__

#include "cocos2d.h"
#include <base/CCGeometry.h>


#include "NodeInfo.h"
#include "Tags.h"

#include "EventManager.h"

#ifdef ENABLE_TOUCH
    #include "TouchEventHandler.h"
#else
    #include "MouseEventHandler.h"
#endif

class EngTestScene : public cocos2d::Layer
{
public:
    
    EngTestScene();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback
    void menuCloseCallback(Object* pSender);
	
	// a selector callback
	void randomizeTables(Object* pSender);
	
    // implement the "static create()" method manually
    CREATE_FUNC(EngTestScene);
 
private:
    
    typedef cocos2d::Point NodePosition;
    typedef cocos2d::Point GridCoordinate;
    
    // World position of all the level nodes
    std::vector<std::vector<NodePosition> > mNodePositions;
    
    // Location of the tables (in grid positions
    std::vector<GridCoordinate> mTableLocations;
    
    // Location of the character in world space
    cocos2d::Point mCharacterPosition;
	
    // mNodeMap is a grid of node identification objects. Its used to make pathing and selection decisions
    std::vector<std::vector<std::vector<NodeInfo *>>> mNodeInfoMap;

	void generateTables(EventListener *_mouseListener, bool random);
	void generateCharacter(EventListener *_mouseListener);
	void defaultTableLayout();
	void removeTables(bool cleanGrid);
	
	void createGridElement(Point gridLocation, EventListener *_mouseListener);
	
	void generateTextLabel(std::string title, std::string font, int fontSize, Point position, int tag);
	
	Point& findFreeGridCoordinate();
	bool verifyMovementOnGrid();
	Node *findCharacter();
	
	EventManager eventManager;
	
	#ifdef ENABLE_TOUCH
    TouchEventHandler *_touchEventHandler;
    #else
    MouseEventHandler *_mouseEventHandler;
    #endif
	
	
    
};

#endif // __ENGTEST_SCENE_H__
