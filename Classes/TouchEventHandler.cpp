//
//  TouchEventHandler.cpp
//  CocosTest
//
//  Created by Barry Gear on 11/15/16.
//
//

#ifdef ENABLE_TOUCH

#include "TouchEventHandler.h"

bool TouchEventHandler::onTouchBegan(Touch* touch, Event *event)
{
}

void TouchEventHandler::onTouchMoved(Touch* touch, Event *event)
{
}

void TouchEventHandler::onTouchCancelled(Touch* touch, Event *event)
{
}

void TouchEventHandler::onTouchEnded(Touch* touch, Event *event)
{
    EventTouch* e = dynamic_cast<EventTouch*>(event);
    
    Point nodeClickPos = Director::getInstance()->convertToUI(touch->getLocation());
    
    auto bounds = e->getCurrentTarget()->getBoundingBox();
    if (bounds.containsPoint(nodeClickPos)){
        if(e->getCurrentTarget()->getUserData()) {
            CCLOG("Event: %s Target:(%5.2f,%5.2f)", "Touch", e->getCurrentTarget()->getPositionX(), e->getCurrentTarget()->getPositionY());
            
            CCLOG("Node: %i, %li", ((NodeInfo *)e->getCurrentTarget()->getUserData())->type, ((NodeInfo *)e->getCurrentTarget()->getUserData())->ordinal);
            
            if(((NodeInfo *)e->getCurrentTarget()->getUserData())->type != NODE_TYPE_TABLE) {
                EventCustom event("move_character");
                event.setUserData(e->getCurrentTarget());
                Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
            }
            
        } else {
            CCLOG("Node: null");
        }
        e->stopPropagation();
    }

}

#endif
