//
//  EventManager.cpp
//  CocosTest
//
//  Created by Barry Gear on 11/16/16.
//
//

#include "EventManager.h"

EventManager::EventManager()
:
animationManager() {
	
	for(int x = 0; x < kGridWidth; x++) {
		closed.push_back(std::vector<bool>());
		for(int y = 0; y < kGridHeight; y++) {
			bool _false = false;
			closed[x].push_back(_false);
		}
	}
}

void EventManager::handleCharacterMovementEvent(EventCustom* event) {
	
	// save the target node the user selected
    Node *destinationNode = (Node *)event->getUserData();
	
	/* Locate character scene. This is done by
		1)	locating the currently running scene
		2)	decending into the child list locating the Character by tag. Unlike iOS which does a recursive search,
			Cocos2d doesn't recurse.
	 
	 This should be replaced with something that passes in *what* received the event.
	*/
    Scene *scene = Director::getInstance()->getRunningScene();
    Node* character = nullptr;
    for (auto it : scene->getChildren())
    {
		character = it->getChildByTag(TAG_CHARACTER);
		if(character != nullptr) {
			continue;
		}
    }
	
	if(character != nullptr && (((NodeInfo *)character->getUserData())->gridLocation != ((NodeInfo *)destinationNode->getUserData())->gridLocation) ) {
		std::vector<NodeInfo *>* pathToWalk = aStar(((NodeInfo *)character->getUserData())->gridLocation,
												    ((NodeInfo *)destinationNode->getUserData())->gridLocation);
		
		//pathFind(((NodeInfo *)character->getUserData())->gridLocation,
		//											   ((NodeInfo *)destinationNode->getUserData())->gridLocation, 0);
		
		// move character
//		character->setAnchorPoint(cocos2d::Point(0,0));
		
		CCLOG("Moving character to position (%2.0f,%2.0f) : ", destinationNode->getPositionX(), destinationNode->getPositionY());
		
		animationManager.moveObjectAlongPath(character, pathToWalk);
	}
}


/*
 
 pathFind
 
 Entry point for generating a vector of waypoints to move the main character. Instead of generating pixel based
 paths, this returns a vector of grid locations. The animation manager moves the main character from its current
 position to each of the grid locations.
 
 */

std::vector<NodeInfo *> *EventManager::pathFind(Point startingPoint, Point endingPoint, int depth) {
	
    // reset traversal data. Set all pathStatus values to kInvalidNode.
    for( int x = 0; x < kGridWidth; x++) {
        for(int y = 0; y < kGridHeight; y++) {
            (*mNodeInfoMap)[x][y].back()->pathStatus = Point(kInvalidNode,kInvalidNode);
        }
    }
    
    std::vector<NodeInfo *> *path = _pathfind(startingPoint, endingPoint, depth);
    
    if(path != nullptr) {
		CCLOG("Displaying Generated Path");
        for(NodeInfo* node : *path) { // dump path
            CCLOG("Node Path : (%2.0f, %2.0f)", node->gridLocation.x, node->gridLocation.y);
        }
    }
    return path;
}

/*
 
 _pathfind
 
 Recursive path finding structure. It scans the immediate neighbors of the starting position for the
 destination position and, if not found, recursively calls each position to continue the search.
 
 */


std::vector<NodeInfo *> *EventManager::_pathfind(Point currentPoint, Point endingPoint, int depth) {

    std::vector<NodeInfo *> *path = _scanNeighbors3x3(currentPoint, endingPoint, depth);
	if(path == nullptr) {
		// didnt find it as a neighbor. start a recursive scan
		for(int x = -1; x < 2; x++) {
            // from above then center then below grid point
            for (int y = -1; y < 2; y++) {
                Point point = Point(currentPoint.x + x, currentPoint.y + y);
                if((point.y >= 0 && point.x >= 0) && (point.x < kGridWidth) && (point.y < kGridHeight)) {
                    if(point == currentPoint) {
                        CCLOG("Ignore current position");
                    } else {
						NodeInfo *pNI_currentNode = (*mNodeInfoMap)[point.x][point.y].back();
						NodeInfo *pNI_currentPoint = (*mNodeInfoMap)[currentPoint.x][currentPoint.y].back();
						CCLOG("Checking node [%2.0f, %2.0f][%i]", point.x, point.y, pNI_currentNode->type);
                        if((pNI_currentNode->pathStatus.x != kInvalidNode)  &&
						   (pNI_currentNode->pathStatus.y == depth)) { // if valid and in depth
							
                            path = _pathfind(point, endingPoint, depth+1);
							
                            if(path != nullptr) {
                                path->push_back(pNI_currentPoint);
                                return path;
                            }
                        }
                    }
                } else {
                    CCLOG("Invalid node");
                }
            }
        }
    }
    return path;
}

/*
 
 _scanNeighbors3x3
 
 Checks a 3x3 grid around the current point looking for the target point. If located, it returns with a path.
 
 */

std::vector<NodeInfo *> *EventManager::_scanNeighbors3x3(Point currentPoint, Point endingPoint, int depth) {
	std::vector<NodeInfo *> *path = nullptr;
	// from left then center to right grid point
	for(int x = -1; x < 2; x++) {
		// from above then center then below grid point
		for (int y = -1; y < 2; y++) {
			
			Point pointToCheck = Point(currentPoint.x + x, currentPoint.y + y);
			path = _checkPoint(pointToCheck, currentPoint, endingPoint, depth);
			if(path != nullptr) {
				return path; // return early if non null
			}
		}
	}
	return nullptr;
}

/*
 
 _scanNeighbors1x1
 
 Checks the 4 neighbors surrounding a point for a match for the endingPoint point. It generates straight paths
 and doesnt cut corners. However it doesn't pick the shortest path since it lacks the direction calculation that A*
 provides;
 
 To check the points, it simply adds 4 Point offsets to the currentPoint and compares it to the target point.
 
 */
std::vector<NodeInfo *> *EventManager::_scanNeighbors1x1(Point currentPoint, Point endingPoint, int depth) {
	std::vector<NodeInfo *> *path = nullptr;
	Point offsets[] = {Point(-1,0), Point(1,0), Point(0,-1), Point(0,1)};
	for(Point offset : offsets) {
		Point pointToCheck = Point(currentPoint.x + offset.x, currentPoint.y + offset.y);
		path = _checkPoint(pointToCheck, currentPoint, endingPoint, depth);
		if(path != nullptr) {
			return path; // return early if non null
		}
	}
	return path;
}

/*
 
 _checkPoint
 
 This validates the point to check ensuring its not a Table or actually within the game grid. If the point to validate
 matches the ending point, it allocates a vector and adds the ending point and the current point to the list and returns.
 Otherwise it returns nullptr. Upon inspecting a node, it sets a pair of values (using a Point). The first value is the unique
 grid number (usually a number from 0 to MaxGridCount) and the depth at which it was marked.
 
 */
std::vector<NodeInfo *> *EventManager::_checkPoint(Point pointToCheck, Point currentPoint, Point endingPoint, int depth) {
	std::vector<NodeInfo *> *path = nullptr;
	if((pointToCheck.y >= 0 && pointToCheck.x >= 0) && (pointToCheck.x < kGridWidth) && (pointToCheck.y < kGridHeight)) {
		NodeInfo *pNI_currentInspectionPoint = (*mNodeInfoMap)[pointToCheck.x][pointToCheck.y].back();
		NodeInfo *pNI_currentPoint = (*mNodeInfoMap)[currentPoint.x][currentPoint.y].back();
		
		if(pointToCheck == currentPoint) {
			CCLOG("Ignore current position");
		} else if(pointToCheck == endingPoint) {
			CCLOG("Found target!");
			path = new std::vector<NodeInfo *>();
			path->push_back(pNI_currentInspectionPoint);
			path->push_back(pNI_currentPoint);
			return path;
		} else {
			CCLOG("Checking node [%2.0f,%2.0f][%i]", pointToCheck.x, pointToCheck.y, pNI_currentInspectionPoint->type);
			if(pNI_currentInspectionPoint->pathStatus.x == kInvalidNode) { // If never visited
				if(pNI_currentInspectionPoint->type != NODE_TYPE_TABLE) {
					pNI_currentInspectionPoint->pathStatus = Point(pNI_currentInspectionPoint->ordinal, depth);
					CCLOG("Setting pathStatus to (%2.0f,%2.0f)", pNI_currentInspectionPoint->pathStatus.x, pNI_currentInspectionPoint->pathStatus.y);
				} else {
					CCLOG("Table in path!");
				}
			}
		}
	} else {
		CCLOG("Invalid node");
	}
	return nullptr;
}

/*
 
 resetAStar
 
 This resets all state variables for A* path finding.
 
 */
void EventManager::resetAStar() {
	for(int x = 0; x < kGridWidth; x++) {
		for(int y = 0; y < kGridHeight; y++) {
			bool _false = false;
			closed[x][y] = _false;
			std::vector<NodeInfo *>ptr =(*mNodeInfoMap)[x][y];
			for(int z = 0; z < ptr.size(); z++) {
				ptr[z]->parent = nullptr;
				ptr[z]->finalCost = 0;
				ptr[z]->heuristicCost = 0;
			}
		}
	}
	
	while(openPQ.empty() != true) {
		openPQ.pop();
	}
}

void EventManager::checkAndUpdateCost(NodeInfo *current, NodeInfo *t, int cost) {
	if((t == nullptr) || closed[t->gridLocation.x][t->gridLocation.y] == true) {
		return;
	}
	
	int t_final_cost = t->heuristicCost + cost;
	
	bool inOpen = false;
	bool result = openPQ.contains(t);
	
	if ( result ) {
		inOpen = true;
	}
	
	if (!inOpen || t_final_cost < t->finalCost) {
		t->finalCost = t_final_cost;
		t->parent = current;
		if(!inOpen) {
			openPQ.push(t);
		}
	}
};


std::vector<NodeInfo *> * EventManager::aStar(Point startPoint, Point endPoint) {
	
	std::vector<NodeInfo *> * path = nullptr;
	
	resetAStar();
	
	for(int i = 0; i < kGridWidth; ++i ) {
		for(int j = 0; j < kGridHeight; ++j ) {
			((*mNodeInfoMap)[i][j].back())->heuristicCost = std::abs(i-endPoint.x) + std::abs(j-endPoint.y);
		}
	}
	((*mNodeInfoMap)[startPoint.x][startPoint.y].back())->finalCost = 0;
	
	//add the start location to open list.
	openPQ.push((*mNodeInfoMap)[startPoint.x][startPoint.y].back());
	
	//NodeInfo *current = nullptr;
	
	while(true) {
		NodeInfo *current = nullptr;
		if(openPQ.size() > 0) {
			current = openPQ.top();
			openPQ.pop();
		}
		if(current == nullptr) {
			break;
		}
		
		if(current->type == NODE_TYPE_TABLE) {
			continue;
		}
		
		closed[current->gridLocation.x][current->gridLocation.y] = true;
		
		if(current == (*mNodeInfoMap)[endPoint.x][endPoint.y].back()) {
			break;
		}
		
		NodeInfo *t;
		if(current->gridLocation.x-1 >= 0) {
			t = (*mNodeInfoMap)[current->gridLocation.x-1][current->gridLocation.y].back();
			checkAndUpdateCost(current, t, current->finalCost+V_H_COST);
			
			if(current->gridLocation.y-1 >= 0) {
				t = (*mNodeInfoMap)[current->gridLocation.x-1][current->gridLocation.y-1].back();
				checkAndUpdateCost(current, t, current->finalCost+DIAGONAL_COST);
			}
			
			if(current->gridLocation.y+1 < kGridHeight) {
				t = (*mNodeInfoMap)[current->gridLocation.x-1][current->gridLocation.y+1].back();
				checkAndUpdateCost(current, t, current->finalCost+DIAGONAL_COST);
			}
		}
		
		if(current->gridLocation.y-1 >= 0) {
			t = (*mNodeInfoMap)[current->gridLocation.x][current->gridLocation.y-1].back();
			checkAndUpdateCost(current, t, current->finalCost+V_H_COST);
		}
		
		if(current->gridLocation.y+1 < kGridHeight){
			t = (*mNodeInfoMap)[current->gridLocation.x][current->gridLocation.y+1].back();
			checkAndUpdateCost(current, t, current->finalCost+V_H_COST);
		}
		
		if(current->gridLocation.x+1 < kGridWidth){
			t = (*mNodeInfoMap)[current->gridLocation.x+1][current->gridLocation.y].back();
			checkAndUpdateCost(current, t, current->finalCost+V_H_COST);
			
			if(current->gridLocation.y-1 >= 0){
				t = (*mNodeInfoMap)[current->gridLocation.x+1][current->gridLocation.y-1].back();
				checkAndUpdateCost(current, t, current->finalCost+DIAGONAL_COST);
			}
			
			if(current->gridLocation.y+1 < kGridHeight){
				t = (*mNodeInfoMap)[current->gridLocation.x+1][current->gridLocation.y+1].back();
				checkAndUpdateCost(current, t, current->finalCost+DIAGONAL_COST);
			}
		}
	}
	
	if(closed[endPoint.x][endPoint.y]) {
		
		path = new std::vector<NodeInfo *>();
		//Trace back the path
		//CCLOG("Path: ");
		NodeInfo *current = (*mNodeInfoMap)[endPoint.x][endPoint.y].back();
		path->push_back(current);
		//CCLOG("[%2.0f,%2.0f]", current->gridLocation.x, current->gridLocation.y);
		while(current->parent != nullptr) {
			path->push_back(current->parent);
			//CCLOG(" -> [%2.0f,%2.0f]", current->parent->gridLocation.x, current->parent->gridLocation.y);
			current = current->parent;
		}
	} else {
		CCLOG("No possible path");
	}
	return path;
}
