//
//  NodeInfo.cpp
//  CocosTest
//
//  Created by Barry Gear on 11/14/16.
//
//

#include "NodeInfo.h"


NodeInfo::NodeInfo(int type, long ordinal, std::vector<std::vector<std::vector<NodeInfo *>>> *_pNodeInfoMap)
: gridLocation(-1, -1), pathStatus(kInvalidNode,kInvalidNode), parent(nullptr)
{	
	this->type = type;
	
	this->ordinal = ordinal;
	
	this->pNodeInfoMap = _pNodeInfoMap;
}
