#include "EngTestScene.h"

#define COCOS2D_DEBUG 1

USING_NS_CC;

EngTestScene::EngTestScene()
:
    mNodePositions(),
    mTableLocations(),
    mCharacterPosition(135,135),
    mNodeInfoMap(),
    eventManager()
{
    mTableLocations.push_back(GridCoordinate(2,1));
    mTableLocations.push_back(GridCoordinate(2,2));
    mTableLocations.push_back(GridCoordinate(2,3));
    mTableLocations.push_back(GridCoordinate(3,3));
    mTableLocations.push_back(GridCoordinate(4,3));
    mTableLocations.push_back(GridCoordinate(5,3));
    mTableLocations.push_back(GridCoordinate(6,3));
    mTableLocations.push_back(GridCoordinate(7,3));
    mTableLocations.push_back(GridCoordinate(7,2));
    mTableLocations.push_back(GridCoordinate(7,1));
    mTableLocations.push_back(GridCoordinate(7,0));
	
#ifdef ENABLE_TOUCH
    _touchEventHandler = new TouchEventHandler();
#else
    _mouseEventHandler = new MouseEventHandler();
#endif

}

Scene* EngTestScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = EngTestScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool EngTestScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
										   "CloseNormal.png",
										   "CloseSelected.png",
										   CC_CALLBACK_1(EngTestScene::menuCloseCallback, this));
	
	closeItem->setPosition(Point(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu, 1);
	
	// add a randomizeTables button
	auto randomizeItem = MenuItemImage::create(
										   "CloseNormal.png",
										   "CloseSelected.png",
										   CC_CALLBACK_1(EngTestScene::randomizeTables, this));
	
	randomizeItem->setPosition(Point(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
									 origin.y + visibleSize.height - closeItem->getContentSize().height/2));
	
	// create menu, it's an autorelease object
	auto randomizeItemMenuItem = Menu::create(randomizeItem, NULL);
	randomizeItemMenuItem->setPosition(Point::ZERO);
	this->addChild(randomizeItemMenuItem, 2);

    /////////////////////////////
    // 3. add your codes below...
	
	this->setTag(TAG_SCENE);
	
#ifdef ENABLE_TOUCH
	auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(TouchEventHandler::onTouchBegan, _touchEventHandler);
    touchListener->onTouchEnded = CC_CALLBACK_2(TouchEventHandler::onTouchEnded, _touchEventHandler);
    touchListener->onTouchMoved = CC_CALLBACK_2(TouchEventHandler::onTouchMoved, _touchEventHandler);
    touchListener->onTouchCancelled = CC_CALLBACK_2(TouchEventHandler::onTouchCancelled, _touchEventHandler);
#else
	EventListenerMouse *_mouseListener = EventListenerMouse::create();
	_mouseListener->onMouseMove = CC_CALLBACK_1(MouseEventHandler::onMouseMove, _mouseEventHandler);
	_mouseListener->onMouseUp = CC_CALLBACK_1(MouseEventHandler::onMouseUp, _mouseEventHandler);
	_mouseListener->onMouseDown = CC_CALLBACK_1(MouseEventHandler::onMouseDown, _mouseEventHandler);
	_mouseListener->onMouseScroll = CC_CALLBACK_1(MouseEventHandler::onMouseScroll, _mouseEventHandler);
#endif
	
	// The eventManager needs the nodeInfo data since path finding is done by nodeInfo lookups.
    eventManager.setNodeInfoMap(&mNodeInfoMap);
	// The animation needs the nodePositions data since navigation is done by moving the character to nodePositions.
	eventManager.getAnimationManager()->setNodePositions(&mNodePositions);
	
	// Add the custom call back to trigger character movement
    EventListenerCustom * _listener = EventListenerCustom::create(MOVE_CHARACTER,
                                                                  CC_CALLBACK_1(EventManager::handleCharacterMovementEvent,
																				eventManager));
	_eventDispatcher->addEventListenerWithFixedPriority(_listener, 1);
    
#ifdef ENABLE_TOUCH
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
#endif
    
	// add "EngTestScene" splash screen"
	auto sprite = Sprite::create("bg.png");
	// position the sprite on the center of the screen
	sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	// add the sprite as a child to this layer
	this->addChild(sprite, 0);

	// add a label shows "Hello World"
    // create and initialize a label
	
	generateTextLabel("Hello World", "Arial", 24, Point(origin.x + visibleSize.width/2,
														origin.y + visibleSize.height), -1);
	
	generateTextLabel("Debug", "Arial", 24, Point(visibleSize.width * .75,
												  origin.y + visibleSize.height), TAG_DEBUG);
	
	generateTextLabel("mouse", "Arial", 24, Point(origin.x + visibleSize.width/4,
												  origin.y + visibleSize.height), TAG_MOUSE_DEBUG);
	
    // Add a node grid
    for (int x = 0 ; x < kGridWidth ; ++x)
    {
        mNodePositions.push_back(std::vector<NodePosition>());
		mNodeInfoMap.push_back(std::vector<std::vector<NodeInfo *>>());
        
        for (int y = 0 ; y < kGridHeight ; ++y)
        {
			createGridElement(GridCoordinate(x,y),_mouseListener);
        }
    }
    
    // Add some tables
	generateTables(_mouseListener, false);
    
    // Add the character
	generateCharacter(_mouseListener);

    return true;
}

void EngTestScene::createGridElement(Point gridLocation, EventListener *_mouseListener) {
	
	int cardinal = gridLocation.y + (gridLocation.x * kGridHeight);
	NodeInfo *nodeInfo = new NodeInfo(NODE_TYPE_TILE, cardinal, &mNodeInfoMap);
	auto nodeSprite = Sprite::create("node.png");
	mNodePositions[gridLocation.x].push_back(
			NodePosition(nodeSprite->getBoundingBox().size.width *gridLocation.x,
						 nodeSprite->getBoundingBox().size.height*gridLocation.y));
	nodeSprite->setPosition(mNodePositions[gridLocation.x][gridLocation.y]);
	nodeSprite->setAnchorPoint(cocos2d::Point(0,0));
	nodeInfo->gridLocation = GridCoordinate(gridLocation.x, gridLocation.y);
	
	nodeSprite->setUserData(nodeInfo);
	nodeSprite->setTag(TAG_TILE + cardinal);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_mouseListener->clone(), nodeSprite);
	
	this->addChild(nodeSprite);
	std::vector<std::vector<NodeInfo *>>*gridRow = &mNodeInfoMap[gridLocation.x];
	gridRow->push_back(std::vector<NodeInfo *>());
	gridRow->back().push_back(nodeInfo); // replace nodeInfo with table
}

void EngTestScene::generateTables(EventListener *_mouseListener, bool random) {
	int i = 0;
	for (auto it : mTableLocations)
	{
		NodeInfo *nodeInfo = new NodeInfo(NODE_TYPE_TABLE, i, &mNodeInfoMap);
		
		auto tableSprite = Sprite::create("table.png");
		tableSprite->setPosition(mNodePositions[it.x][it.y]);
		nodeInfo->gridLocation = GridCoordinate(it.x, it.y);
		tableSprite->setAnchorPoint(cocos2d::Point(0,0));
		tableSprite->setUserData(nodeInfo);
		tableSprite->setTag(TAG_TABLE + i);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(_mouseListener->clone(), tableSprite);
		this->addChild(tableSprite);
		std::vector<NodeInfo *>*gridStack = &mNodeInfoMap[it.x][it.y];
		gridStack->push_back(nodeInfo); // replace nodeInfo with table
		i++;
	}
}

void EngTestScene::generateCharacter(EventListener *_mouseListener) {
	auto characterSprite = Sprite::create("flo.png");
	NodeInfo *nodeInfo = new NodeInfo(NODE_TYPE_PLAYER, 0, &mNodeInfoMap);
	nodeInfo->gridLocation = GridCoordinate(1, 1);
	characterSprite->setPosition(mNodePositions[1][1]);
	characterSprite->setUserData(nodeInfo);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_mouseListener->clone(), characterSprite);
	characterSprite->setAnchorPoint(Point(-.25,0.0));
	characterSprite->setTag(TAG_CHARACTER);
	this->addChild(characterSprite);
	
	//std::vector<NodeInfo *>*gridStack = &mNodeInfoMap[nodeInfo->gridLocation.x][nodeInfo->gridLocation.y];
	//gridStack->push_back(nodeInfo);
}

#pragma mark -

void EngTestScene::defaultTableLayout() {
	
	removeTables(true);
	mTableLocations.push_back(GridCoordinate(2,1));
	mTableLocations.push_back(GridCoordinate(2,2));
	mTableLocations.push_back(GridCoordinate(2,3));
	mTableLocations.push_back(GridCoordinate(3,3));
	mTableLocations.push_back(GridCoordinate(4,3));
	mTableLocations.push_back(GridCoordinate(5,3));
	mTableLocations.push_back(GridCoordinate(6,3));
	mTableLocations.push_back(GridCoordinate(7,3));
	mTableLocations.push_back(GridCoordinate(7,2));
	mTableLocations.push_back(GridCoordinate(7,1));
	mTableLocations.push_back(GridCoordinate(7,0));
}

void EngTestScene::removeTables(bool cleanGrid)
{
	while(!mTableLocations.empty()) {
		mTableLocations.pop_back();
	}
}

void EngTestScene::randomizeTables(Object* pSender)
{
	std::srand(std::time(0));

	for(int tablePieceToMove = 0; tablePieceToMove < mTableLocations.size(); tablePieceToMove++) {
		// now retrive its grid position. This will be used to index the mNodeInfoMap to find the nodeInfo.
		Point tableCoordinate = mTableLocations[tablePieceToMove];
		
		// retrieve the gridRow of NodeInfo entries
		std::vector<NodeInfo *>*gridStack = &mNodeInfoMap[tableCoordinate.x][tableCoordinate.y];
		// now get the highest element in the gridStack of the location
		
		
		// find table Node using tag
		Scene *scene = Director::getInstance()->getRunningScene();
		Node* table = nullptr;
		for (auto it : scene->getChildren())
		{
			table = it->getChildByTag(TAG_TABLE + tablePieceToMove);
			if(table != nullptr) {
				continue;
			}
		}
		NodeInfo *tableNodeInfo = nullptr;
		GridCoordinate destinationGrid;
		int maxTries = 10;
		bool validDestination = true;
		do {
			// begin to move table. first find a new location
			destinationGrid = findFreeGridCoordinate();
			
			tableNodeInfo = gridStack->back();
			CC_ASSERT(tableNodeInfo->type == NODE_TYPE_TABLE);
			// remove it from grid stack
			gridStack->pop_back();

			// update mTablesLocations with new location
			mTableLocations[tablePieceToMove] = destinationGrid;
			
			// update nodeInto gridLocation with location
			tableNodeInfo->gridLocation = destinationGrid;
			// add nodeInfo to gridstack
			gridStack = &mNodeInfoMap[tableNodeInfo->gridLocation.x][tableNodeInfo->gridLocation.y];
			gridStack->push_back(tableNodeInfo);
				
			validDestination = verifyMovementOnGrid();
		}
		while(!validDestination && maxTries-- > 0);
		
        if(maxTries <= 0) {
            gridStack = &mNodeInfoMap[tableCoordinate.x][tableCoordinate.y];
            tableNodeInfo->gridLocation = tableCoordinate;
            gridStack->push_back(tableNodeInfo);
        } else {
            // move it
            eventManager.getAnimationManager()->moveObject(table, tableNodeInfo);
        }
    }
}

Node *EngTestScene::findCharacter() {
	
	Scene *scene = Director::getInstance()->getRunningScene();
	Node* character = nullptr;
	for (auto it : scene->getChildren())
	{
		character = it->getChildByTag(TAG_CHARACTER);
		if(character != nullptr) {
			continue;
		}
	}
	return character;
}

Point& EngTestScene::findFreeGridCoordinate() {
	
	Scene *scene = Director::getInstance()->getRunningScene();
	Node* character = nullptr;
	for (auto it : scene->getChildren())
	{
		character = it->getChildByTag(TAG_CHARACTER);
		if(character != nullptr) {
			continue;
		}
	}
	
	std::vector<GridCoordinate> freespaces;
	
	for (int x = 0 ; x < kGridWidth ; ++x)
	{
		for (int y = 0 ; y < kGridHeight ; ++y)
		{
			std::vector<NodeInfo *>gridStack = mNodeInfoMap[x][y];
			if(gridStack.size() > 0 && gridStack.back()->type == NODE_TYPE_TILE &&
			   gridStack.back()->gridLocation != ((NodeInfo*)(character->getUserData()))->gridLocation) {
				freespaces.push_back(gridStack.back()->gridLocation);
			}
		}
	}
	
	std::random_shuffle(freespaces.begin(), freespaces.end());
	return freespaces.back();
	
}

bool EngTestScene::verifyMovementOnGrid() {
	
	Node *character = findCharacter();
	bool validForAllGridLocatons = true;
	for (int x = 0 ; x < kGridWidth ; ++x)
	{
		for (int y = 0 ; y < kGridHeight ; ++y)
		{
			NodeInfo *currentTarget = mNodeInfoMap[x][y].back();
			if(currentTarget->type == NODE_TYPE_TILE) {
				
				if(!eventManager.aStar(((NodeInfo*)character->getUserData())->gridLocation, currentTarget->gridLocation)) {
					validForAllGridLocatons = false;
					break;
				}
			}
		}
	}
	
	return validForAllGridLocatons;
}

void EngTestScene::generateTextLabel(std::string title, std::string font, int fontSize, Point position, int tag) {
	auto label = LabelTTF::create(title, font, fontSize);
	label->setTag(tag);
	label->setPosition(Point(position.x, position.y - label->getContentSize().height));
	this->addChild(label, 1);
}

void EngTestScene::menuCloseCallback(Object* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
