//
//  EventManager.hpp
//  CocosTest
//
//  Created by Barry Gear on 11/16/16.
//
//

#ifndef EventManager_h
#define EventManager_h

#include "cocos2d.h"

#include "AnimationManager.h"
#include "NodeInfo.h"
#include "Tags.h"

#include "FindablePriorityQueue.hpp"

USING_NS_CC;

const std::string MOVE_CHARACTER = "move_character";

class EventManager
{
public:
    
    EventManager();
    
	AnimationManager *getAnimationManager() { return &animationManager; };

    void handleCharacterMovementEvent(EventCustom *event);
    
	std::vector<NodeInfo *> *pathFind(Point startingPosition, Point endingPosition, int depth);

    void setNodeInfoMap(std::vector<std::vector<std::vector<NodeInfo *>>> *_mNodeInfoMap) { this->mNodeInfoMap = _mNodeInfoMap; };
    
	std::vector<NodeInfo *> * aStar(Point startPoint, Point endPoint);

private:
	const static int DIAGONAL_COST = 14;
	const static int V_H_COST = 10;

	AnimationManager animationManager;

	std::vector<std::vector<std::vector<NodeInfo *>>> *mNodeInfoMap;
	
	std::vector<NodeInfo *> *_pathfind(Point currentPosition, Point endingPosition, int depth);
	std::vector<NodeInfo *> *_scanNeighbors3x3(Point currentPosition, Point endingPosition, int depth);
	std::vector<NodeInfo *> *_scanNeighbors1x1(Point currentPosition, Point endingPosition, int depth);
	
	std::vector<NodeInfo *> *_checkPoint(Point positionToCheck, Point currentPosition, Point endingPosition, int depth);

	FindablePriorityQueue<NodeInfo *> openPQ;
	
	std::vector<std::vector<bool>> closed;
 
	void setBlocked(int i, int j) { (*mNodeInfoMap)[i][j].back()->pathStatus = Point(-1,-1); };
 
	void resetAStar();
	
	void checkAndUpdateCost(NodeInfo *current, NodeInfo *t, int cost);
	
};

#endif /* EventManager_h */
