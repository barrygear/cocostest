//
//  AnimationManager.hpp
//  CocosTest
//
//  Created by Barry Gear on 11/16/16.
//
//

#ifndef AnimationManager_h
#define AnimationManager_h

#include "cocos2d.h"
#include "NodeInfo.h"

USING_NS_CC;


class AnimationManager
{
    typedef cocos2d::Point NodePosition;
    
public:
    
    void moveObject(Node* object, NodeInfo *pNodeInfo);
    
    void moveObjectAlongPath(Node* object, std::vector<NodeInfo *>* path);
    
    void setNodePositions(std::vector<std::vector<NodePosition>>* nodePositions) { pNodePositions = nodePositions; }
    
    void moveFinished(Node* sender, NodeInfo *pNodeInfo);
private:
    std::vector<std::vector<NodePosition>>* pNodePositions;
    
    std::vector<NodeInfo*>* wayPoints;
	
	bool busy = false;
    
};
#endif /* AnimationManager_h */
