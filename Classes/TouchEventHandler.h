//
//  TouchEventHandler.h
//  CocosTest
//
//  Created by Barry Gear on 11/15/16.
//
//

#ifdef ENABLE_TOUCH

#ifndef __ENGTEST_TOUCH_EVENT_HANDLER_H__
#define __ENGTEST_TOUCH_EVENT_HANDLER_H__

#include "cocos2d.h"
#include <base/CCGeometry.h>

USING_NS_CC;

#include "NodeInfo.h"

class TouchEventHandler
{
public:
    
    TouchEventHandler() {};
    
    bool onTouchBegan(Touch* touch, Event* event);
    
    void onTouchMoved(Touch* touch, Event* event);
    
    void onTouchEnded(Touch* touch, Event* event);
    
    void onTouchCancelled(Touch* touch, Event *event);
    
};


#endif /* __ENGTEST_TOUCH_EVENT_HANDLER_H__ */

#endif
