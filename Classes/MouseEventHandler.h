//
//  MouseEventHandler.hpp
//  CocosTest
//
//  Created by Barry Gear on 11/14/16.
//
//

#ifndef __ENGTEST_MOUSE_EVENT_HANDLER_H__
#define __ENGTEST_MOUSE_EVENT_HANDLER_H__

#include "cocos2d.h"

#include "NodeInfo.h"
#include "EventManager.h"
#include "Tags.h"

USING_NS_CC;

class MouseEventHandler
{
public:
	
	MouseEventHandler() {};
	
	void onMouseDown(Event *event);
	
	void onMouseUp(Event *event);

	void onMouseMove(Event *event);
	
	void onMouseScroll(Event *event);
};

#endif // __ENGTEST_MOUSE_EVENT_HANDLER_H__
