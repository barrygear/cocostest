//
//  NodeInfo.hpp
//  CocosTest
//
//	NodeInfo is a class added to the UserData field for each Node object.
//	It contains type information, a uniqueId, the objects grid location, and path finding status
//  Created by Barry Gear on 11/14/16.
//
//

#ifndef __ENGTEST_NODE_INFO_H__
#define __ENGTEST_NODE_INFO_H__

#include "cocos2d.h"

USING_NS_CC;

#define NODE_TYPE_PLAYER    0
#define NODE_TYPE_TILE      1
#define NODE_TYPE_TABLE     2

static const int kInvalidNode = -1;

static const int kStartNode = 0;

class NodeInfo
{
public:
    typedef cocos2d::Point PathFindStatus;
    
	NodeInfo(int type, long ordinal, std::vector<std::vector<std::vector<NodeInfo *>>> *_pNodeInfoMap);
	
	int type;
	
    // A unique identifier. This can be replaced with a GUID or UUID.
    long ordinal;
	
	int heuristicCost = 0; //Heuristic cost
	
	int finalCost = 0; //G+H
	
	NodeInfo *parent;
	
    // Placement in node grid
    Point gridLocation;
    
    // This is for path traversal. It holds the previous ordinal and the search depth.
    PathFindStatus pathStatus;
	
	std::vector<std::vector<std::vector<NodeInfo *>>> *pNodeInfoMap = nullptr;
	
	bool operator()(NodeInfo * const & n0, NodeInfo * const & n1) {
		return n0->finalCost < n1->finalCost;
	}
};

#endif /* __ENGTEST_NODE_INFO_H__ */
