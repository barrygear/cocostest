//
//  AnimationManager.cpp
//  CocosTest
//
//  Created by Barry Gear on 11/16/16.
//
//

#include "AnimationManager.h"
#include "NodeInfo.h"

void AnimationManager::moveObject(Node* object, NodeInfo *pDestinationNodeInfo) {
	
	busy = true;
	
	// Determine speed of the target
    float minDuration = 0.5;
    float maxDuration = 1.0;
    float rangeDuration = maxDuration - minDuration;
    // srand( TimGetTicks() );
    
    Point destination = (*pNodePositions)[pDestinationNodeInfo->gridLocation.x][pDestinationNodeInfo->gridLocation.y];
    
    // Create the actions
    FiniteTimeAction* actionMove = MoveTo::create( (float)rangeDuration, (Point(destination.x, destination.y)) );
    
    auto actionMoveDone = CallFunc::create(
		[this, object, pDestinationNodeInfo](){ moveFinished(object, pDestinationNodeInfo); }
	);
	
//	std::vector<std::vector<std::vector<NodeInfo *>>>nodeMap = *pDestinationNodeInfo->pNodeInfoMap;
//	nodeMap[((NodeInfo*)object->getUserData())->gridLocation.x][((NodeInfo*)object->getUserData())->gridLocation.y].pop_back();
	
	object->runAction( Sequence::create(actionMove, actionMoveDone, NULL) );
}

void AnimationManager::moveObjectAlongPath(Node* object, std::vector<NodeInfo *>* path) {
	
	if(!busy) {
		wayPoints = path;
		wayPoints->pop_back(); // pop the first one
		
		moveObject(object, wayPoints->back());
		wayPoints->pop_back();
	}
}

void AnimationManager::moveFinished(Node *object, NodeInfo *pDestinationNodeInfo) {
    
    ((NodeInfo *)object->getUserData())->gridLocation = pDestinationNodeInfo->gridLocation;
	
//	std::vector<std::vector<std::vector<NodeInfo *>>>nodeMap = *pDestinationNodeInfo->pNodeInfoMap;
//	nodeMap[((NodeInfo*)object->getUserData())->gridLocation.x][((NodeInfo*)object->getUserData())->gridLocation.y].push_back((NodeInfo*)object->getUserData());
	
    if(wayPoints != nullptr && wayPoints->size() > 0) {
        moveObject(object, wayPoints->back());
        wayPoints->pop_back();
	} else {
		busy = false;
	}
}

